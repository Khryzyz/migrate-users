package routines;

import java.util.UUID;

public class FriendshipId {

    public static String uuid() {
    	return UUID.randomUUID().toString();
    }
}
